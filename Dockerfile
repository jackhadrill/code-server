FROM codercom/code-server:latest

RUN sudo sh -c "apt-get update && apt-get install -y \
python3 \
python3-venv \
python3-pip \
&& apt-get full-upgrade -y \
&& apt-get clean"
